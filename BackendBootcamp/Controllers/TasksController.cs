﻿using BackendBootcamp.Logics;
using BackendBootcamp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BackendBootcamp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        #region getuserwithtask tanpa parameter
        // [HttpGet]
        // [Route("GetUserWithTask")]
        // public ActionResult GetUserWithTask()
        // {
        //     try
        //     {
        //         List<object> result = new List<object>();
        //         result = TasksLogic.GetUserWithTask();

        //         return Ok(result);
        //     }
        //     catch (Exception ex)
        //     {
        //         return BadRequest(ex.Message);
        //     }
        // }
        #endregion

        [HttpGet]
        [Route("GetUserWithTask")]
        public ActionResult GetUserWith([FromQuery] string? name = null)
        {
            try
            {
                List<object> result = new List<object>();
                result = TasksLogic.GetUserWithTask(name);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("AddUserWithTask")]
        public ActionResult AddUserWithTask([FromBody] TaskUserInputModel inputModel)
        {
            try
            {
                TasksLogic.AddUserWithTask(inputModel.Task, inputModel.User);

                return StatusCode(201, "created");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
