﻿namespace BackendBootcamp.Models
{
    public class TaskUserInputModel
    {
        public TasksInput Task { get; set; }
        public UsersInput User { get; set; }
    }
}
