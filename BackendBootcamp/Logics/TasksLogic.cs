﻿using BackendBootcamp.Models;
using MySqlX.XDevAPI.Common;
using System.Data.SqlClient;

namespace BackendBootcamp.Logics
{
    public class TasksLogic
    {
        private static string connectionString = "Server=Iskandar;Database=MyDatabase;Trusted_Connection=True;";

        #region getUsetWithTask tanpa parameter
        // public static List<object> GetUserWithTask()
        // {
        //     List<object> users = new List<object>();

        //     using (SqlConnection conn = new SqlConnection(connectionString))
        //     {
        //         conn.Open();

        //         using (SqlCommand cmd = conn.CreateCommand())
        //         {
        //             cmd.Connection = conn;
        //             cmd.CommandText = "SELECT * FROM users INNER JOIN tasks ON users.pk_users_id = tasks.fk_users_id ORDER BY tasks.pk_tasks_id;";

        //             SqlDataReader reader = cmd.ExecuteReader();
        //             int lastUserId = -1;
        //             object? currentUser = null;
        //             List<object>? currentTasks = null;
        //             while (reader.Read())
        //             {
        //                 int userId = (int)reader["fk_users_id"];
        //                 if (userId != lastUserId)
        //                 {
        //                     currentTasks = new List<object>();
        //                     currentUser = new
        //                     {
        //                         tasks = currentTasks,
        //                         pk_users_id = reader["pk_users_id"],
        //                         name = reader["name"]
        //                     };
        //                     lastUserId = userId;
        //                     users.Add(currentUser);
        //                 }

        //                 currentTasks.Add(new
        //                 {
        //                     pk_tasks_id = reader["pk_tasks_id"],
        //                     task_detail = reader["task_detail"]
        //                 });
        //             }

        //             reader.Close();
        //         }

        //         conn.Close();
        //     }

        //     return users;
        // }
        #endregion
        
        public static List<object> GetUserWithTask(string? name = null)
        {
            List<object> users = new List<object>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Connection = conn;
                    if (name != null)
                    {
                        cmd.CommandText = "SELECT * FROM users INNER JOIN tasks ON users.pk_users_id = tasks.fk_users_id WHERE name = '"+name+"' ORDER BY users.pk_users_id;";
                    }
                    else
                    {
                        cmd.CommandText = "SELECT * FROM users INNER JOIN tasks ON users.pk_users_id = tasks.fk_users_id ORDER BY tasks.pk_tasks_id;";
                    }

                    SqlDataReader reader = cmd.ExecuteReader();
                    int lastUserId = -1;
                    object? currentUser = null;
                    List<object>? currentTasks = null;
                    while (reader.Read())
                    {
                        int userId = (int)reader["fk_users_id"];
                        if (userId != lastUserId)
                        {
                            if (currentUser != null)
                            {
                                users.Add(currentUser);
                            }
                            currentTasks = new List<object>();
                            currentUser = new
                            {
                                tasks = currentTasks,
                                pk_users_id = reader["pk_users_id"],
                                name = reader["name"]
                            };
                            lastUserId = userId;
                        }

                        currentTasks.Add(new
                        {
                            pk_tasks_id = reader["pk_tasks_id"],
                            task_detail = reader["task_detail"]
                        });
                    }
                    reader.Close();

                    if (currentUser != null)
                    {
                        users.Add(currentUser);
                    }
                }

                conn.Close();
            }
            return users;
        }

        public static void AddUserWithTask(TasksInput task, UsersInput user)
        {
            string insertUserQuery = "INSERT INTO users (name) VALUES (@name); SELECT SCOPE_IDENTITY();";
            string insertTaskQuery = "INSERT INTO tasks (task_detail, fk_users_id) VALUES (@task_detail, @fk_users_id);";

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                // Insert user and get the generated primary key value
                using (SqlCommand cmd = new SqlCommand(insertUserQuery, con))
                {
                    cmd.Parameters.AddWithValue("@name", user.name);
                    int pk_users_id = Convert.ToInt32(cmd.ExecuteScalar());
                    task.fk_users_id = pk_users_id;
                }

                // Insert task with the corresponding foreign key value
                using (SqlCommand cmd = new SqlCommand(insertTaskQuery, con))
                {
                    cmd.Parameters.AddWithValue("@task_detail", task.task_detail);
                    cmd.Parameters.AddWithValue("@fk_users_id", task.fk_users_id);
                    cmd.ExecuteNonQuery();
                }

                con.Close();
            }
        }

    }
}
